<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix = "form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"/>
        <title>Risultato</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-10 pb-5 mt-5">
                    <div class="text-center">
                        <h3 class="text-info font-italic font-weight-bold">Lista Clienti Negozio</h3>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="col-md-2">ID</th>
                                <th class="col-md-5">Nome</th>
                                <th class="col-md-5">Cognome</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${listaCustomer}" var="customer">
                            <tr>
                                <td>
                                   <c:out value="${customer.idPerson}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.name}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.secondName}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>