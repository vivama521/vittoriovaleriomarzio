package com.gestionebiliardi.beanutil;

public class ControllerUrlRepo {

    private String message;
    private String url;

    public ControllerUrlRepo(String message, String url) {
        this.message = message;
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
